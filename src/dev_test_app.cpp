/***********************************************************************
 *
 *            Copyright 2010 Mentor Graphics Corporation
 *                         All Rights Reserved.
 *
 * THIS WORK CONTAINS TRADE SECRET AND PROPRIETARY INFORMATION WHICH IS
 * THE PROPERTY OF MENTOR GRAPHICS CORPORATION OR ITS LICENSORS AND IS
 * SUBJECT TO LICENSE TERMS.
 *
 ************************************************************************

 ************************************************************************
 *
 * FILE NAME
 *
 *       dev_test_app.cpp
 *
 * DESCRIPTION
 *
 *
 ***********************************************************************/
#include <ctime>
#include <functional>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <thread>
#include <vector>

#include <ctype.h>
#include <signal.h>
#include <unistd.h>

#include <cspeappsdk/cspeappagent/apperror.h>
#include "dev_test_app.h"

#define USE_API_WITH_PAYLOAD

// API Call interval parameter in seconds define the interval between two
// api calls.
const CSP_STRING Application::API_CALL_INTERVAL_PARAM_TAG = "api_call_interval"; 

Application::Application() : AGENT(nullptr), 
    _apiCaller(nullptr), _lastJobId(""), _hostId(""), _appId(""), _jobid(1), api_call_interval(10), isRunning(true) 
{

}

Application::~Application()
{
    isRunning = false;
}

bool Application::initialize()
{
    // AppAgent object is the primary interface to all the workflow operations 
    // performed by the CSP Agent
    this->AGENT = std::unique_ptr<cspeapps::sdk::AppAgent>(new cspeapps::sdk::AppAgent());

    // Before even initializing the Agent, we will register the Signalling Callback because
    // the application will start to receive BE signals as soon as the Agent is initialized
    // to make sure we do not miss any BE signal, we will register our handler now.
    this->AGENT->RegisterBESignalCallback(std::bind(&Application::beSignallingRequest, this, std::placeholders::_1));

    // Register a signal handler for DM signals comming in from Gateway. These will mostly be
    // control signals
    this->AGENT->RegisterDMSignalCallback(std::bind(&Application::dmSignallingRequest, this, std::placeholders::_1));

    // Initialize the agent. 
    this->AGENT->Initialize(std::bind(&Application::initializeResponse, this, std::placeholders::_1));
}

void Application::log(const std::string &msg)
{
    std::time_t now = std::time(nullptr);
    std::stringstream _time_ss;
    _time_ss << "[" << std::put_time(std::localtime(&now), "%c") << "]";
    std::cout << _time_ss.str() << " : [DEV-TEST-APPLICATION] : " << msg << std::endl;
}

CSP_VOID Application::initializeResponse(const INIT_RESPONSE &res)
{
    if ( res.status ) {
        log("Getting Application Configuration from CSP Platform BE");

        // Once we are initialized successfully, our first task is to get the configuration of the application
        // from the BE so that we can start our application. 
        this->AGENT->GetConfiguration(std::bind(&Application::getConfigResponse, this, std::placeholders::_1, ""), "", OPERATION_STATE::NO_OP);
    } else {
        log("Initialization Failed");
    }
}
CSP_VOID Application::getConfigResponse(cspeapps::sdk::AppConfig config, const OPERATION_ID &op_id)
{
    log("Received configuration from CSP Platform BE");
    // We will keep a copy of the Configuration object because we may need this while running the application.
    CONFIG.reset();
    CONFIG = std::unique_ptr<cspeapps::sdk::AppConfig>(new cspeapps::sdk::AppConfig(config));
    cspeapps::sdk::AppResponse response = CONFIG->GetApplicationResponse();
    if ( response.GetStatus() == cspeapps::sdk::C2AMTypes::COMPLETE ) {
        // Apply the new value
        log("Applying requested configuration");
        CSP_STRING reqVal = CONFIG->GetRequestedValue(API_CALL_INTERVAL_PARAM_TAG);
        CSP_STRING curVal = CONFIG->GetCurrentValue(API_CALL_INTERVAL_PARAM_TAG);

        // If requested value is changed, there will be a value, otherwise we will
        // use the current value.
        if ( reqVal.length() > 0 ) {
            api_call_interval = atoi(reqVal.c_str());
        } else {
            api_call_interval = atoi(curVal.c_str());
        }

        // Application specific logic. Start our worker thread here.
        if ( !_apiCaller ) {
            _apiCaller = std::unique_ptr<std::thread>(new std::thread(std::bind(&Application::callApiHandler, this)));
        }

        // Now set the new current value. We will ensure to read the current value of any given parameter
        // from its original source (instead of just using the RequestedValue) to ensure the exact value
        // being used by the application. This will make sure correct reporting of current value at the BE
        log("Setting new current value");
        CONFIG->SetCurrentValue(API_CALL_INTERVAL_PARAM_TAG, std::to_string(api_call_interval), "new value applied");

        // Report back newly applied value
        log("Reporting back newly applied configuration");
        this->AGENT->ReportConfiguration(*CONFIG, nullptr, op_id, OPERATION_STATE::FINISHED);

        // Check if we have updated our configuration as part of BE Signal we will report
        // back the status of the update_configuration signal.
        if ( _lastJobId.length() > 0 ) {
            BE_REQUEST_STATUS reqStatus;
            reqStatus.jobid = _lastJobId;
            reqStatus.job_status = REQ_STATUS_CODE::SUCCESS;
            _lastJobId = "";

            if ( this->AGENT->ReportRequestStatus(reqStatus) ) {
                log("Request [" + reqStatus.jobid + "] status reported successfully");
            } else {
                log("Request [" + reqStatus.jobid + "] status failed to report");
            }
        }
    } else {
        log("Some error occurred while fetch configuration. Error Details = [" + response.GetErrorDetails() + "]");
    }
}
CSP_VOID Application::beSignallingRequest(cspeapps::sdk::AppSignal signal)
{
    // CSP Platform BE Signal handler
    log("Received a signal from BE");
    _lastJobId = signal.GetJobId();

    // Currently we only have one operation as implemented below.
    OPERATION_ID op_id = signal.GetOperationId();
    if ( signal.GetRequestedOperation() == "update_configuration" ) {
        // This operation does not have any parameters, so we are just taking
        // appropriate action to service this request
        // Since the signal is asking us to update the configuration, so we will 
        // just call the GetConfiguration API again.
        log("Getting configuration from BE for Operation Id = [" + op_id + "]");
        this->AGENT->GetConfiguration(std::bind(&Application::getConfigResponse, this, std::placeholders::_1, op_id), op_id, OPERATION_STATE::IN_PROGRESS);
    } else if ( signal.GetRequestedOperation() == "parameter_changed" ) {
        log("Some subscribed parameter has been changed. Take appropriate action");
        cspeapps::sdk::AppSignal::SIG_OP_PARAMS sig_params = signal.GetOperationParams();
        this->AGENT->GetConfiguration(std::bind(&Application::getConfigResponse, this, std::placeholders::_1, op_id), op_id, OPERATION_STATE::IN_PROGRESS);
    } else {
        log("ERROR: Unhandled Signal");
    }
    log("Signal handling completed");
}
CSP_VOID Application::dmSignallingRequest(cspeapps::sdk::DMSignal signal)
{
    CSP_STRING sig_string = signal.GetSignal();
    log("Received DM Signal = [" + sig_string + "]");
    if ( sig_string == "app_queue_idle" ) {
        log("Our application queue is idle now, we can start sending our requests again");
        while ( pendingRequests.size() != 0 ) {
            APP_PACKET packet;
            log("Total requests in the queue = [" + std::to_string(pendingRequests.size()) + "]");
            {
                std::lock_guard<std::mutex> lock(_mPendingRequests);
                packet = pendingRequests.back();
                pendingRequests.pop_back();
            }
            callApi(packet);
            std::this_thread::sleep_for(std::chrono::seconds(api_call_interval));
        }
    }
}
CSP_VOID Application::callApiHandler()
{
    _hostId = this->AGENT->GetHostId();
    _appId = this->AGENT->GetApplicationId();

    log("Application Host Id = " + _hostId);
    log("Initial API Call Interval = " + std::to_string(api_call_interval));
    while ( isRunning ) {
#ifdef USE_API_WITH_PAYLOAD
        callApiWithPayload();
#else
        callApi();
#endif
        std::this_thread::sleep_for(std::chrono::seconds(api_call_interval));
    }
    log("Exiting API Caller");
}
CSP_VOID Application::callApi()
{
    // Call API Here
    // We will call following API here
    // Description = Get gateway connected peripherals
    // Path = /gateway-services/containerapps/v1/gateway/{serialNumber}/peripheral
    // Method = GET
    APP_PACKET packet;
    packet.jobid = createJobId(); 
    packet.method = "GET";
    packet.api = "/gateway-services/containerapps/v1/gateway/" + _hostId + "/peripheral";

    // Make the call
    if ( this->AGENT->BEApiCall(packet, std::bind(&Application::apiCallResponse, this, std::placeholders::_1)) ) {
        log("BE Api Call submitted to the API Call Queue");
    } else {
        log("BE Api Call failed to be submitted to the API Call Queue");
        removeJobId(packet.jobid);
    }
}
CSP_VOID Application::callApiWithPayload()
{
    // Call API Here
    // We will call following API here
    // Description = Get gateway connected peripherals
    // Path = /gateway-services/containerapps/v1/{containerId}/status
    // Method = POST
    // Body = {"status":"Ready", "status_details":"Dev test application with payload"}
    APP_PACKET packet;
    packet.jobid = createJobId(); 
    packet.method = "POST";
    packet.api = "/gateway-services/containerapps/v1/" + _appId + "/status";
    packet.body = "{\"status\":\"Ready\",\"status_details\":\"Dev test application with payload\"}";

    // Make the call
    if ( this->AGENT->BEApiCall(packet, std::bind(&Application::apiCallResponse, this, std::placeholders::_1)) ) {
        log("BE Api Call submitted to the API Call Queue");
    } else {
        log("BE Api Call failed to be submitted to the API Call Queue");
        removeJobId(packet.jobid);
    }
}

CSP_VOID Application::callApi(APP_PACKET packet)
{
    // Make the call
    if ( this->AGENT->BEApiCall(packet, std::bind(&Application::apiCallResponse, this, std::placeholders::_1)) ) {
        log("BE Api Call submitted to the API Call Queue");
    } else {
        log("BE Api Call failed to be submitted to the API Call Queue");
        removeJobId(packet.jobid);
    }
}
CSP_VOID Application::apiCallResponse(const cspeapps::sdk::AppResponse &response)
{
    printApiCallRawResponse(response);
    CSP_STRING jobid = response.GetJobId();
    // removeJobId(jobid);
    cspeapps::sdk::C2AMTypes::STATUS status = response.GetStatus();
    switch(status) {
        case cspeapps::sdk::C2AMTypes::COMPLETE:
        {
            log("C2AM Status = Complete");
        }
        break;
        case cspeapps::sdk::C2AMTypes::BUSY:
        {
            log("We have received a Busy response from DM");
            APP_PACKET packet;
            if ( response.GetApplicationRequestPacket(packet) ) {
                std::lock_guard<std::mutex> lock(_mPendingRequests);
                pendingRequests.push_back(packet);
                log("Pushing our request to local queue. Total Requests = [" + std::to_string(pendingRequests.size()) + "]");
            } else {
                log("Failed to get request in the Busy response from DM");
            }
        }
        break;
        case cspeapps::sdk::C2AMTypes::ERROR:
        {
            log("C2AM Status = Error");
        }
        break;
    }
}
CSP_VOID Application::removeJobId(const CSP_STRING &id)
{
    int temp_id = atoi(id.c_str());
    log("Total Response Remaining = [" + std::to_string(_jobIds.size() - 1) + "]");
    std::lock_guard<std::mutex> lock(_mJobId);
    _jobIds.erase(_jobIds.begin() + temp_id);
}
CSP_STRING Application::createJobId()
{
    log("Sending Request Id = [" + std::to_string(_jobid) + "]");
    std::lock_guard<std::mutex> lock(_mJobId);
    _jobIds.push_back(_jobid);
    CSP_STRING id = std::to_string(_jobid);
    _jobid++;

    return id;
}
CSP_VOID Application::printApiCallRawResponse(const cspeapps::sdk::AppResponse &resp)
{
    CSP_STRING httpResponse = "";
    resp.GetHttpResponseString(httpResponse);

    log(" ===== Application Request Response ===== ");
    log("Type          = [" + std::to_string(resp.GetType()) + "]");
    log("Status        = [" + std::to_string(resp.GetStatus()) + "]");
    log("Error         = [" + std::to_string(resp.GetError()) + "]");
    log("Version       = [" + resp.GetVersion() + "]");
    log("Message       = [" + resp.GetFriendlyMessage() + "]");
    log("Error Detail  = [" + resp.GetErrorDetails() + "]");
    log("Encoding      = [" + resp.GetHttpResponseEncoding() + "]");
    log("Job Id        = [" + resp.GetJobId() + "]");
    log("Direction     = [" + resp.GetDirection() + "]");
    log("HTTP Response = [" + httpResponse + "]");
    log(" ===== Application Request Response End ===== ");
}
