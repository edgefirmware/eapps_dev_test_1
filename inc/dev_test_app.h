/***********************************************************************
*
*            Copyright 2010 Mentor Graphics Corporation
*                         All Rights Reserved.
*
* THIS WORK CONTAINS TRADE SECRET AND PROPRIETARY INFORMATION WHICH IS
* THE PROPERTY OF MENTOR GRAPHICS CORPORATION OR ITS LICENSORS AND IS
* SUBJECT TO LICENSE TERMS.
*
************************************************************************

************************************************************************
*
* FILE NAME
*
*       dev_test_app.h
*
* DESCRIPTION
*
***********************************************************************/
#ifndef _DEV_TEST_APP_H_
#define _DEV_TEST_APP_H_

#include <memory>
#include <mutex>
#include <thread>
#include <cspeappsdk/cspeappagent/appagent.h>
#include <cspeappsdk/cspeappagent/appconfig.h>
#include <cspeappsdk/cspeappagent/dmsignal.h>

class Application {
public:
    Application();
    ~Application();
    bool initialize();
    void log(const std::string &msg);

    // CSP Application Agent Callback Handlers
    CSP_VOID initializeResponse(const INIT_RESPONSE &res);
    CSP_VOID getConfigResponse(cspeapps::sdk::AppConfig config, const OPERATION_ID &op_id);
    CSP_VOID beSignallingRequest(cspeapps::sdk::AppSignal signal);
    CSP_VOID dmSignallingRequest(cspeapps::sdk::DMSignal signal);
private:
    CSP_VOID callApiHandler();
    CSP_VOID callApi();
    CSP_VOID callApiWithPayload();
    CSP_VOID callApi(APP_PACKET packet);
    CSP_VOID apiCallResponse(const cspeapps::sdk::AppResponse &resp);
    CSP_STRING createJobId();
    CSP_VOID removeJobId(const CSP_STRING &id);
    CSP_VOID printApiCallRawResponse(const cspeapps::sdk::AppResponse &resp);
public:
    std::unique_ptr<cspeapps::sdk::AppAgent> AGENT;
    std::unique_ptr<cspeapps::sdk::AppConfig> CONFIG;
    std::unique_ptr<std::thread> _apiCaller;
    std::vector<int> _jobIds;
    std::vector<APP_PACKET> pendingRequests;
    std::mutex _mJobId;
    std::mutex _mPendingRequests;
    CSP_STRING _lastJobId;
    CSP_STRING _hostId;
    CSP_STRING _appId;
    int _request_counter;
    int _response_counter;
    int _jobid;
    volatile int api_call_interval;
    volatile bool isRunning;
private:
    static const CSP_STRING API_CALL_INTERVAL_PARAM_TAG; 
};

#endif /* _DEV_TEST_APP_H_ */
